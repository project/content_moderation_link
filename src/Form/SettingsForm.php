<?php

namespace Drupal\content_moderation_link\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Content Moderation Link settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_moderation_link_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['content_moderation_link.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('content_moderation_link.settings');
    $form['allow_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple IDs'),
      '#default_value' => $config->get('allow_multiple'),
      '#description' => $this->t('Allow moderation links to specify multiple values, comma separated. If this is unchecked, only the first value willbe used.'),
      '#required' => FALSE,
    ];
    $form['skip_errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip errors'),
      '#default_value' => $config->get('skip_errors'),
      '#description' => $this->t("When processing multiple values, skip ids that can't be successfully resolved. If unchecked the processing will halt on an invalid id."),
      '#required' => FALSE,
    ];
    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination Route'),
      '#placeholder' => '<front>',
      '#default_value' => $config->get('destination'),
      '#required' => FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('content_moderation_link.settings');
    // Values to skip.
    $values_to_skip = [
      'submit',
      'form_build_id',
      'form_token',
      'form_id',
      'op',
    ];

    foreach ($form_state->getValues() as $key => $value) {
      if (!in_array($key, $values_to_skip)) {
        $config->set($key, $value);
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
