<?php

namespace Drupal\content_moderation_link\Controller;

use Drupal\content_moderation\StateTransitionValidationInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Content Moderation Link routes.
 */
class ContentModerationLinkController extends ControllerBase {
  use MessengerTrait;

  /**
   * The state transition validation service.
   *
   * @var \Drupal\content_moderation\StateTransitionValidationInterface
   */
  protected $validator;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\content_moderation\StateTransitionValidationInterface $validator
   *   State transition validator.
   */
  public function __construct(StateTransitionValidationInterface $validator) {
    $this->validator = $validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_moderation.state_transition_validation')
    );
  }

  /**
   * Attempt to moderate the specified content, and redirect.
   */
  public function moderate($state, $type, $id) {
    // ex. /content-moderation-link/process/published/node/{id}
    // ex. /content-moderation-link/process/in_review/node/108
    // ex. /content-moderation-link/process/draft/node/108
    // ex. /content-moderation-link/process/in_review/node/108,109
    // ex. /content-moderation-link/process/in_review/node/88,107.
    // Require login.
    $account = $this->currentUser();
    if ($account->isAnonymous()) {
      return $this->redirect('user.login', $this->getDestinationArray());
    }

    $config = $this->config('content_moderation_link.settings');
    $skip_errors = $config->get('skip_errors');
    $destination = $config->get('destination') ?: '<front>';

    $storage = $this->entityTypeManager()->getStorage($type);

    if (!$storage) {
      // Log error and escape.
      $this->messenger()->addError('Unable to use the specified entity type.');
      // @todo Redirect to configured destination.
      return $this->redirect($destination);
    }

    $processed = [];

    // Parse multiple values.
    $parsed_ids = explode(',', $id);
    if (!$config->get('allow_multiple')) {
      $parsed_ids = array_slice($parsed_ids, 0, 1);
    }

    // Step through ids.
    // @todo Prevalidate IDs and process none if one is invalid and $skip_errors
    // is false.
    foreach ($parsed_ids as $parsed_id) {
      $entity = $storage->load($parsed_id);
      if (!$entity) {
        // Throw error.
        $message = $this->t('ID @id is invalid for a @type entity.', [
          '@id' => $parsed_id,
          '@type' => $type,
        ]);
        if ($skip_errors) {
          $this->messenger()->addWarning($message);
          continue;
        }
        else {
          $this->postMessages($processed, $type, $state);
          $this->messenger()->addError($message);
          return $this->redirect($destination);
        }
      }

      // Access check.
      $is_valid = FALSE;

      $valid_transitions = $this->validator->getValidTransitions($entity, $account);
      foreach ($valid_transitions as $transition) {
        if ($transition->to()->id() == $state) {
          $is_valid = TRUE;
          break;
        }
      }
      if (!$is_valid) {
        // Throw warning.
        $message = $this->t('Attempt to moderate @type entity @id without valid transition.', [
          '@id' => $parsed_id,
          '@type' => $type,
        ]);
        $this->messenger()->addWarning($message);
        continue;
      }

      // Change state.
      $entity->set('moderation_state', $state);

      // Allow other modules to alter the entity.
      \Drupal::moduleHandler()->invokeAll('content_moderation_link_alter_entity', [&$entity]);

      // Allow other modules to alter the account.
      \Drupal::moduleHandler()->invokeAll('content_moderation_link_alter_account', [&$account]);

      // Set revision log, if configured.
      if ($entity instanceof RevisionLogInterface) {
        // @todo populate from config.
        $entity->setRevisionLogMessage('Changed moderation state to @state via Content Moderation Link.', ['@state' => $state]);
        $entity->setRevisionUserId($account->id());
      }

      // Save.
      $entity->save();

      // Log processed data.
      $processed[] = ['id' => $parsed_id, 'title' => $entity->label()];

      // Continue ids.
    }

    $this->postMessages($processed, $type, $state);

    // @todo Redirect to a configured destination.
    return $this->redirect($destination);
  }

  /**
   * {@inheritdoc}
   */
  public function postMessages($processed, $type, $state) {
    if ($processed) {
      $this->messenger()->addStatus('The following content was successfully processed:');
      foreach ($processed as $data) {
        $message = $this->t('Moderated @type @id to @state: @title', [
          '@id' => $data['id'],
          '@type' => $type,
          '@state' => $state,
          '@title' => $data['title'],
        ]);
        $this->messenger()->addStatus($message);
      }
    }
  }

}
